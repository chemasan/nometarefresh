.PHONY: clean

NoMetaRefresh.zip: removemetarefresh.js manifest.json
	zip $@ $^

clean:
	rm -f NoMetaRefresh.zip
